#include "commons.glsl"
const int Steps = 1000;
const float Epsilon = 0.01; // Marching epsilon
const float T=0.5;

const float rA=10.0; // Maximum and minimum ray marching or sphere tracing distance from origin
const float rB=40.0;

// Transforms
vec3 rotateX(vec3 p, float a)
{
  float sa = sin(a);
  float ca = cos(a);
  return vec3(p.x, ca*p.y - sa*p.z, sa*p.y + ca*p.z);
}

vec3 rotateY(vec3 p, float a)
{
  float sa = sin(a);
  float ca = cos(a);
  return vec3(ca*p.x + sa*p.z, p.y, -sa*p.x + ca*p.z);
}

vec3 rotateZ(vec3 p, float a)
{
  float sa = sin(a);
  float ca = cos(a);
  return vec3(ca*p.x + sa*p.y, -sa*p.x + ca*p.y, p.z);
}

/**
  Translation
*/
vec3 Translation(vec3 p, vec3 t) {
  return vec3(p.x + t.x, p.y + t.y, p.z + t.z);
}

/**
  Scale | Homothesie
*/
vec3 Scale(vec3 p, vec3 s) {
  return vec3(p.x / s.x, p.y / s.y, p.z / s.z);
}

// Smooth cubic falloff function
// x : distance
// R : radius
float falloff(float x, float R)
{
  float u = clamp(x/R,0.0,1.0);
  float v = (1.0-u*u);
  return v*v*v;
}

// Primitives

// Point skeleton
// p : point
// c : center of skeleton
// e : energy associated to skeleton
// R : radius associated to skeleton
float point(vec3 p, vec3 c, float e,float R)
{
  return e*falloff(length(p-c),R);
}

vec4 point(vec3 p, vec3 c, float e,float R, vec3 color)
{
  float a = e*falloff(length(p-c),R);
  return vec4(a, color);
}
/** Segment
  p : point
  a : first point of the segment
  b : second point of the segment
  e : energy associated to skeleton
  R : radius associated to skeleton
*/
vec4 Segment(vec3 p, vec3 a, vec3 b, float e, float R, vec3 color)
{
  vec3 ab = b - a; 
  vec3 ap = p - a;
  vec3 bp = p - b;
  float l = length(ab);
  vec3 u = ab / l;
  float ah = dot(ap, u);
  float d;
  if(ah < 0.0f) // before a
    d = dot(ap, ap);
  else if(ah > l) // after b
    d = dot(bp, bp);
  else  // between a and b
    d = dot(ap, ap) - pow(ah, 2.0);
  return vec4(e * falloff(d, R), color);
}

/** Circle
  p: point
  c: center of the circle
  n: normal of the circle
  r: radius of the circle
  e: energy associated to skeleton
  R: radius associated to skeleton
*/
vec4 Circle(vec3 p, vec3 c, vec3 n, float r, float e, float R, vec3 color) {
  float ph = dot((p - c), n);
  float hc = sqrt(dot((p-c), (p-c)) - pow(ph, 2.0));
  float hq = hc - r;
  float d = pow(ph, 2.0) + pow(hq, 2.0); 
  return vec4(e*falloff(d, R), color);
}

/** Disc
  p: point
  c: center of the circle
  n: normal of the circle
  r: radius of the circle
  e: energy associated to skeleton
  R: radius associated to skeleton
*/
vec4 Disc(vec3 p, vec3 c, vec3 n, float r, float e, float R, vec3 color) {
  float ph = dot((p - c), n);
  float hc = sqrt(dot((p-c), (p-c)) - pow(ph, 2.0));
  float hq;
  if(hc > r)
    hq = hc - r;
  else
    hq = 0.0;
  float d = pow(ph, 2.0) + pow(hq, 2.0); 
  return vec4(e * falloff(d, R), color);
}

/** Cylinder
  https://liris.cnrs.fr/Documents/Liris-1297.pdf
  p: point
  a: first point of the cylinder
  b: second point of the cylinder
  r: radius of the cylinder
  e: energy associated to skeleton
  R: radius associated to skeleton
*/
vec4 Cylinder(vec3 p, vec3 a, vec3 b, float r, float e, float R, vec3 color) {
  vec3 ab = b - a;
  float l = length(ab);
  vec3 u = ab / l;
  float d;
  float ah = dot(p - a, u);

  float x = dot(p - a, u);
  float y_2 = pow(length(p - a), 2.0) - pow(x, 2.0);
  float y = sqrt(y_2);
  float r_2 = pow(r, 2.0);
  if(x < 0.0) {
    // before a
    d = pow(x, 2.0);
    if(!(y_2 < r_2)) {
      d = pow(y - r, 2.0) + d;
    }
  } else if(x < l) {
    // betwenn a and b
    if(y_2 < r_2) {
      d = 0.0;
    } else {
      // Inside the cylinder
      d = pow(y - r, 2.0);
    }
  } else if(x > l) {
    // after b
    d = pow(x - l, 2.0);
    if(!(y_2 < r_2)) {
      d = pow(y - r, 2.0) + d;
    }
  }
  
  return vec4(e * falloff(d, R), color);
}
/** Cone
  https://liris.cnrs.fr/Documents/Liris-1297.pdf
  p: point
  a: first point of the cylinder
  ra: radius of the circle with a as center
  b: second point of the cylinder
  rb: radius of the circle with b as center
  e: energy associated to skeleton
  R: radius associated to skeleton
*/
/*
vec4 Cone(vec3 p, vec3 a, float ra, vec3 b, float rb, float e, float R, vec3 color) {
  float d = 0.5;
  vec3 ab = b - a;
  float l = length(ab);
  vec3 u = ab / l;
  float x = dot(p - a, u);
  float y_2 = pow(x, 2.0) + pow(length(p - a), 2.0);
  float y = sqrt(y_2);

  float ra_2 = pow(ra, 2.0);
  float rb_2 = pow(rb, 2.0);
  
  if(x < 0.0f) { // before a
      d = pow(x, 2.0);
    if(!(y_2 < ra_2)) //
      d = pow(y - ra, 2.0) + d;
  } else if(y_2 < ra_2) { // after b
    if(x > l)
      d = pow(x - l, 2.0);
    else
      d = 0.0;
  } else { // between a and b
    float delta_r = ra - rb;
    float s = sqrt(pow(l, 2.0) + pow(delta_r, 2.0));
    vec3 i = vec3(0, delta_r / s, l / s);
    vec3 j = vec3(l / s, - (delta_r / s), 0);
  }

  return vec4(e * falloff(d, R), color);
}
*/

/** Cube
  https://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm
  p: point
  b: scale of the Cube
  e: energy associated to skeleton
  R: radius associated to skeleton
*/
vec4 Cube(vec3 p, vec3 b, float e, float R, vec3 color) {
  vec3 q = abs(p) - b;
  float d =  length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
  return vec4(e * falloff(d, R), color);
}

/** Plan
  https://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm
  p: Point
  n: normal of the plan
  h: height of the plan
  e: energy associated to skeleton
  R: radius associated to skeleton
*/
vec4 Plan(vec3 p, vec3 n, float h, float e, float R, vec3 color) {
  float d = dot(p, n) - h;
  return vec4(e * falloff(d, R), color);
}

// Operators

// Blending
// a,b : field function of the sub-trees
float Blend(float a,float b)
{
    return a+b;
}

vec4 Blend(vec4 a, vec4 b) {
  return vec4(a.x + b.x,
    clamp((a.x * a.yzw + b.x * b.yzw) / (a.x + b.x), 0.0, 1.0)
  );
}

/** Blend *
*/
float Blend_univ(float a, float b, float e) {
  return pow(pow(a, e) + pow(b, e), 1.0 / e);
}

// Union
// a : field function of left sub-tree
// b : field function of right sub-tree
float Union(float a,float b)
{
    return max(a,b);
}

vec4 Union(vec4 a, vec4 b) {
  float u = max(a.x, b.x);

  return vec4(
    u,
    (u == a.x ? a.yzw : b.yzw)
  );
}

/** Intersection
  a : field function of left sub-tree
  b : field function of right sub-tree
*/
float Intersection(float a,float b)
{
    return min(a,b);
}

vec4 Intersection(vec4 a, vec4 b) {
  float i = min(a.x, b.x);
  return vec4(i,
    (i == a.x ? a.yzw : b.yzw) 
  );
}

/** Substraction
  a : field function of left sub-tree
  b : field function of right sub-tree
  T : 
*/
float Substraction(float a, float b, float T) {
  return min(a, (2.0 * T) - b);
}

vec4 Substraction(vec4 a, vec4 b, float T) {
  float s = min(a.x, (2.0 * T) - b.x);

  return vec4(s,
    (s == a.x ? a.yzw : b.yzw)
  );
}

/** SCENES OBJECTS **/
vec4 moon(vec3 p, vec3 color) {
  vec4 v = point(p, vec3(0.0, 0.0, 0.0), 1.0, 3.5, color);
  v = Blend(v, point(p, vec3(-1.0, 0.0, -1.0), -1.0, 0.5, color));
  v = Blend(v, point(p, vec3(-1.5, 0.5, 0.25), -1.0, 0.5, color));
  v = Blend(v, point(p, vec3(0.0, -1.5, -1.0), -1.0, 0.5, color));
  v = Blend(v, point(p, vec3(-1.0, -1.4, 0.0), -1.0, 0.5, color));
  v = Blend(v, point(p, vec3(1.0, 0.0, 1.0), -1.0, 0.5, color));
  v = Blend(v, point(p, vec3(1.0, 1.4, 0.0), -1.0, 0.5, color));
  v = Blend(v, point(p, vec3(0.0, 1.5, 1.0), -1.0, 0.5, color));
  v = Blend(v, point(p, vec3(-0.5, 1.5, -1.0), -1.0, 0.75, color));
  v = Blend(v, point(p, vec3(1.5, -0.25, -1.0), -1.0, 0.75, color));
  return vec4(v.x,
    clamp(v.yzw, 0.0, 1.0)
  );
}

vec4 asteroids(vec3 p, vec3 color) {
  vec4 v = Disc(p, vec3(0.0, 0.0, 0.0),
    vec3(0.0, 0.0, 1.0), 4.0, 1.0, 0.05, color);
  v = Blend(v, Disc(p, vec3(0.0, 0.0, 0.0),
    vec3(0.0, 0.0, 1.0), 3.0, -1.0, 0.05, color));
  return vec4(v.x,
    clamp(v.yzw, 0.0, 1.0)
  );
}

vec4 space_ship(vec3 p, vec3 color) {
  // p = Translation(p, vec3 (1.0, -20.0, -2.0));
  // p = rotateZ(p, 2.0);
  // p = rotateY(p, 2.0);
  // vec4 v =  Cube(p, vec3(2.0, 4.0, 0.5), 1.0, 1.0, vec3(0.25, 0.25, 0.25));
  vec4 v = Disc(p,
    vec3(0.0, 0.0, 0.0),
    vec3(0.0, 0.0, 1.0), 5.0, 1.0, 1.0,
    color);
  v = Blend(v, 
    Disc(Translation(p, vec3(0.0, 0.0, 0.75)),
      vec3(0.0, 0.0, 0.0),
      vec3(0.0, 0.0, 1.0), 4.0, 1.0, 0.25,
      color)
  );
  v = Blend(v, 
    Disc(Translation(p, vec3(0.0, 0.0, -0.75)),
      vec3(0.0, 0.0, 0.0),
      vec3(0.0, 0.0, 1.0), 4.0, 1.0, 0.25,
      color)
  );
  v = Blend(v, 
    Segment(p
      ,vec3(-5.0, 1.5, 0.0), vec3(-7.0, 1.5, 0.0),1.0,1.0, color)
  );
  v = Blend(v, 
    Segment(p
      ,vec3(-5.0, -1.5, 0.0), vec3(-7.0, -1.5, 0.0),1.0,1.0, color)
  );
  v = Blend(v, 
    Segment(p
      ,vec3(0.0, 0.0, 0.0), vec3(-4.5, 5.0, 0.0),1.0,1.0, color)
  );
  v = Blend(v, 
    Segment(p
      ,vec3(-4.5, 5.0, 0.0), vec3(-6.0, 5.0, 0.0),1.0,1.0, color)
  );
  return v;
}

// Potential field of the object
// p : point
vec4 Object(vec3 p)
{
  vec3 p_hard_noise = p + noise_sum(p, 2.0, 0.25, 0.2);
  vec3 p_noise = p + noise_sum(p, 1.0, 0.050, 0.25);
  p.z=-p.z;

  vec4 v = moon(p_noise, vec3(0.501, 0.250, 0.0));
  v = Union(v, asteroids(rotateY(p_hard_noise, 0.5), vec3(0.55, 0.55, 0)));
  vec3 rotated_p = Scale(p, vec3(0.07, 0.07, 0.07));
  rotated_p = Translation(rotated_p, vec3(35.0, 12.0, 4.0));
  rotated_p = rotateZ(
  rotateX(
    rotateY(rotated_p, -1.75), -0.05
  ), -2.0);
  v = Union(v, space_ship(rotated_p, vec3(0.25)));

  return vec4(v.x-T,
    clamp(v.yzw, 0.0, 1.0)
  );
}

// Calculate object normal
// p : point
vec3 ObjectNormal(in vec3 p )
{
  float eps = 0.0001;
  vec3 n;
  float v = Object(p).x;
  n.x = Object( vec3(p.x+eps, p.y, p.z) ).x - v;
  n.y = Object( vec3(p.x, p.y+eps, p.z) ).x - v;
  n.z = Object( vec3(p.x, p.y, p.z+eps) ).x - v;
  return normalize(n);
}

// Trace ray using ray marching
// o : ray origin
// u : ray direction
// rB : distance max à laquelle on cherche l'intersection
// h : hit
// s : Number of steps
vec4 Trace(vec3 o, vec3 u, float rB, out bool h,out int s)
{
  h = false;

  // Don't start at the origin, instead move a little bit forward
  float t = rA;
  vec4 v; 
  for(int i=0; i<Steps; i++)
  {
    s=i;
    vec3 p = o+t*u;
    v = Object(p);
    
    // Hit object
      if (v.x > 0.0) // dedans
      {
          s=i;
          h = true;
          break;
      }
      // Move along ray
      t += Epsilon;
      // Escape marched far away
      if (t>rB)
      {
          break;
      }
  }
  return vec4(t, v.yzw);
}

// Trace ray using ray marching
// o : ray origin
// u : ray direction
// h : hit
// s : Number of steps
vec4 SphereTrace(vec3 o, vec3 u, float rB, out bool h,out int s)
{
  h = false;

    // Don't start at the origin, instead move a little bit forward
    float t=rA;
    vec4 v;
  for(int i=0; i<Steps; i++)
  {
    s=i;
    vec3 p = o+t*u;
    v = Object(p);
    // Hit object
      if (v.x > 0.0)
      {
          s=i;
          h = true;
          break;
      }
      // Move along ray
      // t += max(Epsilon,abs(v.x)/4.0); // abs(v) -> pour avoir une valeur positive 
      t += max(Epsilon,abs(v.x)/10.0); // abs(v) -> pour avoir une valeur positive 
          // si lambda trop petit: se ratte sur la profondeur de l'objet
      // Escape marched far away
      if (t>rB)
      {
          break;
      }
  }
  return vec4(t, v.yzw);
}


// Ambient occlusion
// p : Point
// n : Normal
// a : Number of smaples
float AmbientOcclusion(vec3 p,vec3 n, int a)
{
    if (a==0) { return 1.0; }
    
	float ao=0.0; 
    
    for (int i=0;i<a;i++)
    {
 		vec3 d = Cosine(581.123*float(i) , n);

        int s;
        bool h;
        float t=SphereTrace(p,d,10.0,h,s).x;
        if (!h) {ao+=1.0;}
        else if (t>5.0)
            {
             ao+=1.0;   
            }
    }
    
    ao/=float(a);
	return ao;
}

// Background color
vec3 background(vec3 rd)
{
  return mix(vec3(0.25, 0.25, 0.25), vec3(0.0, 0.0, 0.25), rd.z*0.5+0.5);
  // return mix(vec3(0.652,0.451,0.995),vec3(0.552,0.897,0.995), rd.z*0.5+0.5);
}


float Light(vec3 p,vec3 n)
{
   // point light
  const vec3 lp = vec3(5.0, 10.0, 25.0);

  vec3 l = normalize(lp - p);

  // Not even Phong shading, use weighted cosine instead for smooth transitions
  float diff = pow(0.5*(1.0+dot(n, l)),2.0);

    bool h;
    int s;
    // float t=SphereTrace(p+0.1*n,l,100.0,h,s).x;
if (!h)
    {
     return diff;
    }
    return 0.0; 
}

float SmoothLight(vec3 p,vec3 n,int a)
{
   if (a==0)
        return 1.0;
    
    // point light
  const vec3 lp = vec3(5.0, 10.0, 25.0);

  vec3 l = normalize(lp - p);
    
float lo=0.0; 
    
    for (int i=0;i<a;i++)
    {
 vec3 d = Cosine(   581.123*float(i) , n);
 d=normalize(l+d*0.15);
        int s;
        bool h;
        float t=SphereTrace(p,d,10.0,h,s).x;
        if (!h) {lo+=1.0;}
        else if (t>100.0)
            {
             lo+=1.0;   
            }
    }
    
    lo/=float(a);
return lo;

}

// Shading and lighting
// p : point,
// n : normal at point
// color: color of the object
vec3 Shade(vec3 p, vec3 n, vec3 color)
{
    // 0.25 = gris foncé
    // vec3 c = objet p.yzw
    // vec3 c = 0.25+0.25*background(n);
    vec3 c = 0.75 * color + 0.25 * background(n);
    c+=0.15*AmbientOcclusion(p+0.1*n,n,15)*vec3(1.0,1.0,1.0);
    c+=0.35*Light(p,n);
    return c;
}

vec3 Shade(vec3 p, vec3 n)
{
  // 0.25 = gris foncé
  vec3 color = vec3(0.25, 0.25, 0.25);
  return Shade(p, n, color);
}

// Shading with number of steps
vec3 ShadeSteps(int n)
{
   float t=float(n)/(float(Steps-1));
   return 0.5+mix(vec3(0.05,0.05,0.5),vec3(0.65,0.39,0.65),t);
}

// Picture in picture
// pixel : Pixel
// pip : Boolean, true if pixel was in sub-picture zone
vec2 Pip(in vec2 pixel, out bool pip)
{
    // Pixel coordinates
    vec2 p = (-iResolution.xy + 2.0*pixel)/iResolution.y;
   if (pip==true)
   {    
    const float fraction=1.0/4.0;
    // Recompute pixel coordinates in sub-picture
    if ((pixel.x<iResolution.x*fraction) && (pixel.y<iResolution.y*fraction))
    {
        p=(-iResolution.xy*fraction + 2.0*pixel)/(iResolution.y*fraction);
        pip=true;
    }
       else
       {
           pip=false;
       }
   }
   return p;
}


// Image
void mainImage( out vec4 color, in vec2 pxy )
{
    // Picture in picture on
    bool pip=true;
    
   // Pixel
     vec2 pixel=Pip(pxy, pip);
    
    // Mouse
    vec2 m=iMouse.xy/iResolution.xy;

    // Camera
    vec3 ro,rd;
    Ray(m,pixel,ro,rd);

  // Trace ray

  // Hit and number of steps
  bool hit;
  int s;

  vec4 t = SphereTrace(ro, rd, 100.0,hit, s);
  
    // Position 
    vec3 pt = ro + t.x * rd;
    
  // Shade background
  vec3 rgb = background(rd);

  if (hit)
  {
    // Compute normal
    vec3 n = ObjectNormal(pt);

    // Shade object with light
    rgb = Shade(pt, n, t.yzw);
  }

  // Uncomment this line to shade image with false colors representing the number of steps
  if (pip==true)
  {
      rgb = ShadeSteps(s); 
  }
  // rgb = ShadeSteps(s); 


  color=vec4(rgb, 1.0);
}

