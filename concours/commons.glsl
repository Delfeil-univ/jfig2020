// Hashing function
// Returns a random number in [-1,1]
float Hash(float seed)
{
    return fract(sin(seed)*43758.5453 );
}

// Cosine direction 
vec3 Cosine( in float seed, in vec3 nor)
{
    float u = Hash( 78.233 + seed);
    float v = Hash( 10.873 + seed);

    	// method 3 by fizzer: http://www.amietia.com/lambertnotangent.html
        float a = 6.2831853 * v;
        u = 2.0 * u - 1.0;
        return normalize( nor + vec3(sqrt(1.0-u*u) * vec2(cos(a), sin(a)), u) );
}


// Rotation matrix around z axis
// a : Angle
mat3 rotate_X(float a)
{
  float sa = sin(a);
  float ca = cos(a);
  return mat3(
        1.0, 0.0, 0.0,
        0.0, ca, sa,
        0.0, -sa, ca
  );
}

mat3 rotate_Y(float a)
{
  float sa = sin(a);
  float ca = cos(a);
  return mat3(
        ca, 0.0, -sa,
        0.0, 1.0, 0.0,
        sa, 0.0, ca
  );
}

mat3 rotate_z(float a)
{
    float sa = sin(a); float ca = cos(a); 
    return mat3(
        ca,sa,0.0,
        -sa,ca,0.0,
        0.0,0.0,1.0
    );
}

// Compute the ray
// m : Mouse position
// p : Pixel
// ro, rd : Ray origin and direction
void Ray(in vec2 m, in vec2 p,out vec3 ro,out vec3 rd)
{
    float a = 3.0*3.14*m.x;
   	float le = 3.8;
    
    // ro=vec3(20.0,0.0,5.0);
    ro=vec3(30.0,-20.0,20.0);
    // ro*=rotate_z(3.0*3.14*m.x);
    ro*=rotate_X(-0.2);
    ro*=rotate_Y(-0.1);
    // ro*=rotate_Y(3.0*3.14*m.y);
    ro*=rotate_z(4.0);

    vec3 ta = vec3(0.0,0.0,1.0);
    vec3 ww = normalize( ta - ro );
    vec3 uu = normalize( cross(ww,vec3(0.0,0.0,1.0) ) );
    vec3 vv = normalize( cross(uu,ww));
	rd = normalize( p.x*uu + p.y*vv + le*ww );
}
