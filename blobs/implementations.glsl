// #include "image.glsl"
// PRIMITIVES

/** Segment
  p : point
  a : first point of the segment
  b : second point of the segment
  e : energy associated to skeleton
  R : radius
*/
float Segment(vec3 p, vec3 a, vec3 b, float e, float R)
{
  vec3 ab = b - a; 
  vec3 ap = p - a; 
    vec3 u = (ab) / length(ab);
    float ah = dot((ap), u);
    if(ah < 0.0f)
        return e*falloff(dot(ap, ap), R);
    else if(ah > length(ab))
        return e*falloff(dot(p - b, p - b), R);
    else
        return e*falloff(dot(ap, ap) - pow(ah, 2.0), R);
}

/** Circle
  p: point
  c: center of the circle
  n: normal of the circle
  r: radius of the circle
  e: energy associated to skeleton
  R: radius
*/
float Circle(vec3 p, vec3 c, vec3 n, float r, float e, float R) {
  float ph = dot((p - c), n);
  float hc = sqrt(dot((p-c), (p-c)) - pow(ph, 2.0));
  float hq = hc - r;
  float d = pow(ph, 2.0) + pow(hq, 2.0); 
  return e*falloff(d, R);
}

/** Disc
  p: point
  c: center of the circle
  n: normal of the circle
  r: radius of the circle
  e: energy associated to skeleton
  R: radius
*/
float Disc(vec3 p, vec3 c, vec3 n, float r, float e, float R) {
  float ph = dot((p - c), n);
  float hc = sqrt(dot((p-c), (p-c)) - pow(ph, 2.0));
  float hq;
  if(hc > r)
    hq = hc - r;
  else
    hq = 0.0;
  float d = pow(ph, 2.0) + pow(hq, 2.0); 
  return e*falloff(d, R);
}

// OPERATIONS

/** Intersection
  a : field function of left sub-tree
  b : field function of right sub-tree
*/
float Intersection(float a,float b)
{
    return min(a,b);
}

// float noise(vec3 p) {

// }


// SCENES
float scene(vec3 p) {
    p.z=-p.z;
    float v=Segment(p,vec3(0.0, 0.0, -1.0), vec3(0.0, 0.0, -4.0),1.0,4.5);
  // v=Blend(v,point(p,vec3(0.0, 2.0, 2.0),1.0, 4.0));
  // v=Blend(v,point(p,vec3(0.0, -2.0, 2.0),1.0, 4.5));
  // Circle
  // float v = Circle(p, vec3(0.0, 0.0, 0.0), vec3(0.50, -0.5, -0.50), 2.5, 1.0, 4.5);

  return v-T;
} 