# Modélisation Géométriques

## Blobs

<img src="blobs/blobs.png">
<a href="https://www.shadertoy.com/view/3sGyRh">Lien vers ShaderToy</a>

Projet du cours de Modélisation Géométriques de Mr E.Galins

Modélisation d'une scène à partir d'une surface implicite.

## JFIG

<img src="concours/ressources/full.png">

Modélisation d'une scène pour un concours de rendu orgénisé par l'Association d'Informatique Graphique, lors des <a href="https://jfig2020.sciencesconf.org/resource/page/id/1">Journées Françaises d'Informatiques Graphiques de 2020</a>

<a href="https://www.shadertoy.com/view/wdtfDn">Lien vers ShaderToy</a>