#include "commons.glsl"

// Based on the shader developped proposed by mr Galin Eric
  // https://www.shadertoy.com/view/wtc3R8
const int Steps = 500;
const float Epsilon = 0.01; // Marching epsilon
const float T=0.5;

const float rA=10.0; // Maximum and minimum ray marching or sphere tracing distance from origin
const float rB=40.0;

const vec3 GREEN = vec3(0.635, 0.815, 0.247) * 0.4;
const vec3 GRAY = vec3(0.690, 0.690, 0.690) * 0.3;

// Transforms
vec3 rotateX(vec3 p, float a)
{
  float sa = sin(a);
  float ca = cos(a);
  return vec3(p.x, ca*p.y - sa*p.z, sa*p.y + ca*p.z);
}

vec3 rotateY(vec3 p, float a)
{
  float sa = sin(a);
  float ca = cos(a);
  return vec3(ca*p.x + sa*p.z, p.y, -sa*p.x + ca*p.z);
}

vec3 rotateZ(vec3 p, float a)
{
  float sa = sin(a);
  float ca = cos(a);
  return vec3(ca*p.x + sa*p.y, -sa*p.x + ca*p.y, p.z);
}

/**
  Translation
*/
vec3 Translation(vec3 p, vec3 t) {
  return vec3(p.x + t.x, p.y + t.y, p.z + t.z);
}

/**
  Scale | Homothesie
*/
vec3 Scale(vec3 p, vec3 s) {
  return vec3(p.x / s.x, p.y / s.y, p.z / s.z);
}

// Primitives
// Based on the signed distance field primitives detailed by Inigo Quilez
  // https://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm
// Sphère
vec4 sdSphere( vec3 p, float s, vec3 color )
{
  return vec4(length(p)-s, color);
}

// Plan
vec4 sdPlane( vec3 p, vec3 n, float h, vec3 color )
{
  // n must be normalized
  return vec4(dot(p,n) + h, color);
}
vec4 sdBox( vec3 p, vec3 b, vec3 color )
{
  vec3 q = abs(p) - b;
  float d = length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
  return vec4(d, color);
}

vec4 sdTorus( vec3 p, vec2 t, vec3 color )
{
  vec2 q = vec2(length(p.xz)-t.x,p.y);
  float d = length(q)-t.y;
  return vec4(d, color);
}

vec4 sdCircle(vec3 p, vec3 c, vec3 n, float r, vec3 color) {
  float ph = dot((p - c), n);
  float hc = sqrt(dot((p-c), (p-c)) - pow(ph, 2.0));
  float hq = hc - r;
  float d = pow(ph, 2.0) + pow(hq, 2.0); 
  return vec4(d, color);
}

vec4 sdCappedTorus(in vec3 p, in vec2 sc, in float ra, in float rb, vec3 color)
{
  p.x = abs(p.x);
  float k = (sc.y*p.x>sc.x*p.y) ? dot(p.xy,sc) : length(p.xy);
  float d = sqrt( dot(p,p) + ra*ra - 2.0*ra*k ) - rb;
  return vec4(d, color);
}

vec4 sdVerticalCapsule( vec3 p, float h, float r, vec3 color )
{
  p.y -= clamp( p.y, 0.0, h );
  float d = length( p ) - r;
  return vec4(d, color);
}

// Operators

// Union
// a : field function of left sub-tree
// b : field function of right sub-tree
vec4 sdUnion(vec4 a, vec4 b) {
  float u = min(a.x, b.x);

  return vec4(
    u,
    (u == a.x ? a.yzw : b.yzw)
  );
}

/** Intersection
  a : field function of left sub-tree
  b : field function of right sub-tree
*/
vec4 sdIntersection(vec4 a, vec4 b) {
  float i = max(a.x, b.x);
  return vec4(i,
    (i == a.x ? a.yzw : b.yzw) 
  );
}

/** Substraction
  a : field function of left sub-tree
  b : field function of right sub-tree
*/
vec4 sdSubstraction(vec4 a, vec4 b) {
  float s = max(a.x, -b.x);
  return vec4(s,
    (s == a.x ? a.yzw : b.yzw)
  );
}

/** SCENES OBJECTS **/

vec4 logo(vec3 p) {
  float z = -5.0;
  float rotate_sin = sin((iTime) * 0.5);
  float rotate_cos = cos((iTime) * 0.5);
  float rotate_sin_2 = sin((iTime +1.0) * 2.0);
  float rotate_cos_2 = cos((iTime +1.0) * 2.0);

  vec4 grayPart = sdTorus(
      rotateX(
        Translation(p, vec3(0.0, 0.0, z))
      , 1.58)
    , vec2(5.0, 0.5), GRAY);
  grayPart = sdSubstraction(grayPart, 
    sdBox(
      rotateX(
        Translation(p, vec3(-10.0, 0.0, z))
      , 0.0)
    , vec3(10.0, 10.0, 1.0), GRAY)
  );

  vec4 greenPart = sdTorus(
      rotateX(
        Translation(p, vec3(0.0, 0.0, z))
      , 1.58)
    , vec2(5.0, 0.5), GREEN);
  greenPart = sdSubstraction(greenPart, 
    sdBox(
      rotateX(
        Translation(p, vec3(10.0, 0.0, z))
      , 0.0)
    , vec3(10.0, 10.0, 1.0), GREEN)
  );

  vec4 v = sdUnion(grayPart, greenPart);

  v = sdSubstraction(v,
    sdSphere(Translation(p,
      vec3(-6.0 * rotate_sin, 6.0 * rotate_cos, z)
      ), 4.0, GRAY)
  );

  v = sdSubstraction(v,
    sdBox(
      rotateX(
        Translation(p, vec3(0.0, 6.0, z))
      , 0.0)
    , vec3(1.0, 20.0, 1.0), GRAY)
  );

  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(-6.0, 0.0, z))
      , 1.5)
    , vec3(1.0, 20.0, 1.0), GRAY)
  );

  v = sdUnion(v,
    sdSphere(Translation(p,
      vec3(-6.0 * rotate_sin, 6.0 * rotate_cos, z)
    ), 2.0, GREEN)
  );

  v = sdUnion(v,
    sdSphere(
      Translation(p, vec3(-11.0 * rotate_sin_2, 11.0* rotate_cos_2, z)
    ), 1.0, GREEN)
  );

  return v;
}

vec4 J(vec3 p) {
  vec4 v =
    sdBox(
      rotateZ(
        Translation(p, vec3(-1.0, 0.0, -0.9))
      , 1.5)
    , vec3(1.0, 2.0, 4.0), GRAY);
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(-2.5, 0.0, -4.0))
      , 1.5)
    , vec3(2, 1.5, 2.5), GREEN)
  );
  v = sdUnion(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(0.0, 0.0, -7.0))
      , 1.5)
    , vec3(1.0, 1.0, 1.0), GREEN)
  );

  return v;
}

vec4 F(vec3 p) {
  vec4 v =
    sdBox(
      rotateZ(
        Translation(p, vec3(1.75, 0, -0.9))
      , 1.5)
    , vec3(1.0, 3.0, 7.0), GRAY);
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(3.0, -0.15, -4.9))
      , 1.5)
    , vec3(2.0, 2.5, 1.0), GREEN)
  );
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(3.0, -0.15, -0.9))
      , 1.5)
    , vec3(2.0, 2.5, 1.0), GREEN)
  );
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(3.75, -0.15, -2.9))
      , 1.5)
    , vec3(2.0, 1.75, 1.5), GREEN)
  );

  return v;
}

vec4 I(vec3 p) {
  vec4 v =
    sdBox(
      rotateZ(
        Translation(p, vec3(0.0, 0.0, -0.9))
      , 1.5)
    , vec3(1.0, 1.0, 4.0), GRAY);
  v = sdUnion(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(0.0, 0.0, -7.0))
      , 1.5)
    , vec3(1.0, 1.0, 1.0), GREEN)
  );

  return v;
}

vec4 G(vec3 p) {
  vec4 v = 
    sdBox(
      rotateZ(
        Translation(p, vec3(0.0, 0.0, -0.9))
      , 1.5)
    , vec3(1.0, 4.0, 7.0), GRAY);
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(0.0, 0.0, -2.0))
      , 1.5)
    , vec3(2.0, 2.0, 0.75), GREEN)
  );
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(-1.0, 0.0, -4.0))
      , 1.5)
    , vec3(2, 1.0, 2.0), GREEN)
  );
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(1.5, 0.0, -5.0))
      , 1.5)
    , vec3(2, 3.5, 1.0), GREEN)
  );

  return v;
}

vec4 Dos(vec3 p) {
  vec4 v = 
    sdBox(
      rotateZ(
        Translation(p, vec3(0.0, 0.0, -0.9))
      , 1.5)
    , vec3(1.0, 2.0, 5.0), GRAY);
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(1.0, 0.0, -1.5))
      , 1.5)
    , vec3(2.0, 1.5, 0.75), GREEN)
  );
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(-1.0, 0.0, -4.0))
      , 1.5)
    , vec3(2.0, 1.5, 0.5), GREEN)
  );
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(1.5, 0.0, -6.0))
      , 1.5)
    , vec3(2.0, 1.0, 1.5), GREEN)
  );

  return v;
}

vec4 Zero(vec3 p) {
  vec4 v = 
    sdBox(
      rotateZ(
        Translation(p, vec3(0.0, 0.0, -0.9))
      , 1.5)
    , vec3(1.0, 2.5, 5.0), GRAY);
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(0.0, 0.0, -3.0))
      , 1.5)
    , vec3(2.0, 1.0, 1.75), GREEN)
  );
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(2.0, 0.0, -6.25))
      , 1.5)
    , vec3(2.0, 1.0, 1.5), GREEN)
  );
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(-2.0, 0.0, -6.25))
      , 1.5)
    , vec3(2.0, 1.0, 1.5), GREEN)
  );
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(2.0, 0.0, 0.25))
      , 1.5)
    , vec3(2.0, 1.0, 1.5), GREEN)
  );
  v = sdSubstraction(v,
    sdBox(
      rotateZ(
        Translation(p, vec3(-2.0, 0.0, 0.25))
      , 1.5)
    , vec3(2.0, 1.0, 1.5), GREEN)
  );

  return v;
}

// Potential field of the object
// p : point
vec4 Object(vec3 p)
{
  vec4 v = sdPlane(p, vec3(0.0, 0.0, 1.0), 0.0, vec3(0.25));
  
  v= sdUnion(v, logo(
    rotateZ(
    Translation(
      rotateX(p, 0.25), vec3(0.0, -2.0, .0)
    ), 2.5)
  ));

  v = sdUnion(v, J(
    Translation(p, vec3(-8.0, 25.0, 0.0))
  ));
  v = sdUnion(v, F(
    Translation(p, vec3(-4.0, 25.0, 0.0))
    ));

  v= sdUnion(v, I(
    Translation(p, vec3(4.0, 25.0, 0.0))
  ));
  v= sdUnion(v, G(
    Translation(p, vec3(11.5, 25.0, 0.0))
  ));
  

  // v= sdUnion(v, Dos(
  //   Scale(
  //     Translation(p, vec3(-4.0, 15.0, 0.0)),
  //   vec3(0.5))
  // ));
  // v= sdUnion(v, Dos(
  //   Scale(
  //     Translation(p, vec3(4.0, 15.0, 0.0)),
  //   vec3(0.5))
  // ));

  // v = sdUnion(v, Zero(
  //   Scale(
  //     Translation(p, vec3(0.0, 15.0, 0.0)),
  //   vec3(0.5))
  // ));
  
  // v = sdUnion(v, Zero(
  //   Scale(
  //     Translation(p, vec3(8.0, 15.0, 0.0)),
  //   vec3(0.5))
  // ));
  

  return vec4(v.x,
    clamp(v.yzw, 0.0, 1.0)
  );
}

// Calculate object normal
// p : point
vec3 ObjectNormal(in vec3 p )
{
  float eps = 0.0001;
  vec3 n;
  float v = Object(p).x;
  n.x = Object( vec3(p.x+eps, p.y, p.z) ).x - v;
  n.y = Object( vec3(p.x, p.y+eps, p.z) ).x - v;
  n.z = Object( vec3(p.x, p.y, p.z+eps) ).x - v;
  return normalize(n);
}

// Trace ray using ray marching
// o : ray origin
// u : ray direction
// h : hit
// s : Number of steps
vec4 SphereTrace(vec3 o, vec3 u, float rB, out bool h,out int s)
{
  h = false;

    // Don't start at the origin, instead move a little bit forward
    float t=rA;
    vec4 v;
  for(int i=0; i<Steps; i++)
  {
    s=i;
    vec3 p = o+t*u;
    v = Object(p);
    // Hit object
      if (v.x < 0.0)
      {
          s=i;
          h = true;
          break;
      }
      // Move along ray
      t += max(Epsilon,abs(v.x)/2.0); // abs(v) -> pour avoir une valeur positive
          // si lambda trop petit: se ratte sur la profondeur de l'objet
      // Escape marched far away
      if (t>rB)
      {
          break;
      }
  }
  return vec4(t, v.yzw);
}

// Shadows
  // Based on an Inigo Quilez's article
    // https://www.iquilezles.org/www/articles/rmshadows/rmshadows.htm
float calcSoftshadow( in vec3 ro, in vec3 rd, in float mint, in float tmax )
{
	float res = 1.0;
    float t = mint;
    float ph = 1e10; // big, such that y = 0 on the first iteration
    for( int i=0; i<16; i++ )
    {
		float h = Object( ro + normalize(rd)*t ).x;
        // traditional technique
        res = min( res, 30.0*h/t );
        t += h;
        if( res<0.0001 || t>tmax ) break; 
    }
    return clamp( res, 0.0, 1.0 );
}

// Ambient occlusion
// p : Point
// n : Normal
// a : Number of smaples
float AmbientOcclusion(vec3 p,vec3 n, int a)
{
    if (a==0) { return 1.0; }
    
	float ao=0.0; 
    
    for (int i=0;i<a;i++)
    {
 		vec3 d = Cosine(581.123*float(i) , n);

        int s;
        bool h;
        float t=SphereTrace(p,d,10.0,h,s).x;
        if (!h) {ao+=1.0;}
        else if (t>5.0)
            {
             ao+=1.0;   
            }
    }
    
    ao/=float(a);
	return ao;
}

// Background color
vec3 background(vec3 rd)
{
  return mix(vec3(0.25, 0.25, 0.25), vec3(0.0, 0.0, 0.25), rd.z*0.5+0.5);
}


float Light(vec3 p,vec3 n, vec3 lp)
{
   // point light

  vec3 l = normalize(lp - p);

  // Not even Phong shading, use weighted cosine instead for smooth transitions
  float diff = pow(0.5*(1.0+dot(n, l)),2.0);

    bool h;
    int s;
// float t=SphereTrace(p+0.1*n,l,100.0,h,s).x;
     return diff;
}

// Shading and lighting
// p : point,
// n : normal at point
// color: color of the object
vec3 Shade(vec3 p, vec3 n, vec3 color, vec3 ro, vec3 rd)
{
    // vec3 col = vec3(0.0);
    vec3 lp = vec3(5.0, 15.0, 10.0);
    vec3  hal = normalize( lp-rd );
    float dif = Light(p, n, lp);
    dif *= calcSoftshadow( p, lp, 0.1, 15.0);

    float spe = pow( clamp( dot( n, hal ), 0.0, 1.0 ),16.0)*
      dif
      * (0.04 + 0.96*pow( clamp(1.0+dot(hal,rd),0.0,1.0), 5.0 ));
    vec3 c = color * 3.75*dif*vec3(0.75);
    c +=      5.0*spe*vec3(0.75);


    // float ao = AmbientOcclusion(p+0.1*n,n,15);
    float amb = clamp( 0.5+0.5*n.z, 0.0, 1.0 );
    c += color*amb*vec3(0.5);
    return c;
}

// Shading with number of steps
vec3 ShadeSteps(int n)
{
   float t=float(n)/(float(Steps-1));
   return 0.5+mix(vec3(0.05,0.05,0.5),vec3(0.65,0.39,0.65),t);
}

// Picture in picture
// pixel : Pixel
// pip : Boolean, true if pixel was in sub-picture zone
vec2 Pip(in vec2 pixel, out bool pip)
{
    // Pixel coordinates
    vec2 p = (-iResolution.xy + 2.0*pixel)/iResolution.y;
   if (pip==true)
   {    
    const float fraction=1.0/4.0;
    // Recompute pixel coordinates in sub-picture
    if ((pixel.x<iResolution.x*fraction) && (pixel.y<iResolution.y*fraction))
    {
        p=(-iResolution.xy*fraction + 2.0*pixel)/(iResolution.y*fraction);
        pip=true;
    }
       else
       {
           pip=false;
       }
   }
   return p;
}

uint[] jfig_bitfield = uint[]( 0x0u,0x0u,0x0u,0xf97800u,0x90900u,0xc91800u,0x890900u,0xf90900u,0x180u,0x0u,0x30e30e0u,0x4904900u,0x49e49e0u,0x4824820u,0x31e31e0u,0x0u,0x0u,0x0u );
bool jfig(in uint x, in uint y) {
    if (x>=32u || y>=18u) return false;
    uint id = x + (17u-y)*32u;
    return 0u != (jfig_bitfield[id/32u] & (1u << (id&31u)));
}

// Image
void mainImage( out vec4 color, in vec2 pxy )
{
    // Picture in picture on
    bool pip=false;
    
   // Pixel
     vec2 pixel=Pip(pxy, pip);
    
    // Mouse
    vec2 m=iMouse.xy/iResolution.xy;

    // Camera
    vec3 ro,rd;
    Ray(m,pixel,ro,rd);

  // Trace ray

  // Hit and number of steps
  bool hit;
  int s;

  vec4 t = SphereTrace(ro, rd, 100.0,hit, s);
  
    // Position 
    vec3 pt = ro + t.x * rd;
    
  // Shade background
  vec3 rgb = background(rd);

  if (hit)
  {
    // Compute normal
    vec3 n = ObjectNormal(pt);

    // Shade object with light
    rgb = Shade(pt, n, t.yzw, ro, rd);
  }

  // Uncomment this line to shade image with false colors representing the number of steps
  if (pip==true)
  {
      rgb = ShadeSteps(s); 
  }
  // rgb = ShadeSteps(s); 

  color=vec4(rgb, 1.0);
  vec2 coord = pxy/iResolution.xy*vec2(32u, 18u)*6.;
  if (jfig(uint(coord.x), uint(coord.y)))
        color.xyz = GREEN*2.0;
}

