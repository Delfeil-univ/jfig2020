// Hashing function
// Returns a random number in [-1,1]
float Hash(float seed)
{
    return fract(sin(seed)*43758.5453 );
}

// Cosine direction 
vec3 Cosine( in float seed, in vec3 nor)
{
    float u = Hash( 78.233 + seed);
    float v = Hash( 10.873 + seed);

    	// method 3 by fizzer: http://www.amietia.com/lambertnotangent.html
        float a = 6.2831853 * v;
        u = 2.0 * u - 1.0;
        return normalize( nor + vec3(sqrt(1.0-u*u) * vec2(cos(a), sin(a)), u) );
}


// Rotation matrix around z axis
// a : Angle
mat3 rotate_X(float a)
{
  float sa = sin(a);
  float ca = cos(a);
  return mat3(
        1.0, 0.0, 0.0,
        0.0, ca, sa,
        0.0, -sa, ca
  );
}

mat3 rotate_Y(float a)
{
  float sa = sin(a);
  float ca = cos(a);
  return mat3(
        ca, 0.0, -sa,
        0.0, 1.0, 0.0,
        sa, 0.0, ca
  );
}

mat3 rotate_z(float a)
{
    float sa = sin(a); float ca = cos(a); 
    return mat3(ca,sa,0.0,    -sa,ca,0.0,  0.0,0.0,1.0);
}

// Compute the ray
// m : Mouse position
// p : Pixel
// ro, rd : Ray origin and direction
void Ray(in vec2 m, in vec2 p,out vec3 ro,out vec3 rd)
{
    float a = 3.0*3.14*m.x;
   	float le = 3.8;
    
    ro=vec3(20.0,0.0,5.0);
    // ro*=rotate_z(3.0*3.14*m.x);
    ro*=rotate_X(3.0*3.14*m.y);
    ro*=rotate_Y(3.0*3.14*m.y);
    ro*=rotate_z(3.0*3.14*m.x);

    vec3 ta = vec3(0.0,0.0,1.0);
    vec3 ww = normalize( ta - ro );
    vec3 uu = normalize( cross(ww,vec3(0.0,0.0,1.0) ) );
    vec3 vv = normalize( cross(uu,ww));
	rd = normalize( p.x*uu + p.y*vv + le*ww );
}

/** Noise function
  https://www.shadertoy.com/view/4ttSWf
*/
/** Value noise
*/
float noise( in vec3 x )
{
    /* Found random values */
    vec3 p = floor(x);
    vec3 w = fract(x);
    
    vec3 u = w*w*w*(w*(w*6.0-15.0)+10.0);
    
    float n = p.x + 317.0*p.y + 157.0*p.z;
    
    float a = Hash(n+0.0);
    float b = Hash(n+1.0);
    float c = Hash(n+317.0);
    float d = Hash(n+318.0);
    float e = Hash(n+157.0);
	float f = Hash(n+158.0);
    float g = Hash(n+474.0);
    float h = Hash(n+475.0);
    
    /* Interpollation */
    float k0 =   a;
    float k1 =   b - a;
    float k2 =   c - a;
    float k3 =   e - a;
    float k4 =   a - b - c + d;
    float k5 =   a - c - e + g;
    float k6 =   a - b - e + f;
    float k7 = - a + b + c - d + e - f - g + h;

    return -1.0+2.0*(k0 + k1*u.x + k2*u.y + k3*u.z + k4*u.x*u.y + k5*u.y*u.z + k6*u.z*u.x + k7*u.x*u.y*u.z);
}

/** Somme de bruits
    p: point
    n: nombre d'itérations
    a: amplitude
    l: longueur d'onde
*/
float noise_sum(vec3 p, float n, float a0, float l0) {
    
    float noise_sum = 0.0;
    for(float i = 0.0; i < n; i++) {
        float a = a0 / pow(2.0, i);
        float l = l0 / pow(2.0, i);
        noise_sum += (noise(p / l) * a);
    }
    return noise_sum;
}